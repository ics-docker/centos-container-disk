FROM alpine:latest
ADD https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2.xz /
RUN unxz /CentOS-7-x86_64-GenericCloud.qcow2.xz

FROM scratch
COPY --from=0 /CentOS-7-x86_64-GenericCloud.qcow2 /disk/
